//
//  Service.swift
//  ShoesPhotography
//
//  Created by Everis on 5/02/22.
//

import Foundation
import Alamofire

struct ShoesResponse : Decodable {
    let status: String?
    let msg_id: String?
    let number_records: Int?
    var objetos: [ObjectList]?
}

struct ObjectList: Decodable {
    let storeId: Int?
    let name: String?
    let price: String?
    let score: String?
    let isFavorite: String?
    let url: String?
    let userId: Int?
}

struct FooRequestParameters : Codable {}

struct Constant {
    static var shoesResponse: ShoesResponse!
    static var urlString = ""
    static var settingNumber = 0
}

class NetworkManager: NSObject {
    func listStore(reload: (() -> Void)!){
        Alamofire.request(Constant.urlString , method: .get, parameters: nil, encoding: URLEncoding.default)
            .validate(statusCode: 200..<300)
            .responseData { response in
                switch response.result {
                case .failure(_):
                    print("Error")
                case .success(let data):
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let result = try decoder.decode(ShoesResponse.self, from: data)
                        Constant.shoesResponse = result
                        reload()
                    } catch { print("Error") }
                }
        }
    }
    
    func createAccount(name: String, lastName: String, email: String, userName: String, password: String, success: (() -> Void)!) {
        
        let parameters: [String: String] = [
            "name": name,
            "lastName": lastName,
            "email": email,
            "userName": userName,
            "password": password
        ]
                
        Alamofire.request("http://ec2-52-55-109-80.compute-1.amazonaws.com:8081/RsCatalog/users" , method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseData { response in
                switch response.result {
                case .failure(_):
                    print("Error")
                case .success(_):
                    success()
                }
        }
    }
    
    func loginAccount(userName: String, password: String, success: (() -> Void)!, showMessage: (() -> Void)!) {
        
        let parameters: [String: String] = [
            "userName": userName,
            "password": password
        ]

        Alamofire.request("http://ec2-52-55-109-80.compute-1.amazonaws.com:8081/RsCatalog/users/authenticate" , method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseData { response in
                print(response)
                switch response.result {
                case .failure(_):
                    let statusCode = (response.response?.statusCode)!
                    if statusCode == 401 {
                        showMessage()
                    }
                    print("Error")
                case .success(_):
                    success()
                }
        }
    }
}
