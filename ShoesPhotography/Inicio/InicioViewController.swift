//
//  InicioViewController.swift
//  ShoesPhotography
//
//  Created by Everis on 2/3/22.
//

import UIKit
import AVFoundation

class InicioViewController: UIViewController , AVCaptureMetadataOutputObjectsDelegate{

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var qrCode = String()
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }

    @IBAction func takePhotoButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let qrcode = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
                qrcode.modalPresentationStyle = .overCurrentContext
                qrcode.modalTransitionStyle = .crossDissolve
                qrcode.delegate = self
        self.present(qrcode, animated: false, completion: nil)
    }
}

extension InicioViewController: QRDelegate {
    func back() {
        self.tabBarController?.selectedIndex = 1
    }
}
