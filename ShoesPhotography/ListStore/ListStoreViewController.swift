//
//  ListStoreViewController.swift
//  ShoesPhotography
//
//  Created by Everis on 4/02/22.
//

import UIKit

class ListStoreViewController: UIViewController {

    @IBOutlet weak var viewOrderList: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var sortBy: UIView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewNoURL: UIView!
    @IBOutlet weak var btnInicio: UIButton!
    @IBOutlet weak var btnOrderList: UIButton!
    
    let dataNew: ShoesResponse? = nil
    let networkManager = NetworkManager()

    override func viewWillDisappear(_ animated: Bool) {
        Constant.urlString = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewOrderListSetting()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        viewBack.isHidden = true
        
        listTableView.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.pressView(_:)))
        self.viewBack.addGestureRecognizer(gesture)
        
        if Constant.urlString == "" {
            viewNoURL.isHidden = false
            btnOrderList.isHidden = true
        } else {
            let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
            myActivityIndicator.center = view.center
            myActivityIndicator.hidesWhenStopped = false
            myActivityIndicator.tag = 100
            myActivityIndicator.transform = CGAffineTransform(scaleX: 2, y: 2)
            view.addSubview(myActivityIndicator)
            myActivityIndicator.startAnimating()
            
            viewNoURL.isHidden = true
            btnOrderList.isHidden = false
            networkManager.listStore(reload: {
                if let viewWithTag = self.view.viewWithTag(100) {
                        viewWithTag.removeFromSuperview()
                    }else{
                        print("No!")
                    }
                self.tableViewSettings()
            })
        }
        super.viewWillAppear(animated)
    }
    
    func tableViewSettings() {
        listTableView.isHidden = false
        listTableView.delegate = self
        listTableView.dataSource = self
        listTableView.reloadData()
    }
    
    func viewOrderListSetting() {
        sortBy.layer.borderWidth = 1
        sortBy.layer.borderColor = UIColor.black.cgColor

        viewOrderList.backgroundColor = .clear
        viewOrderList.layer.cornerRadius = viewOrderList.frame.size.height / 2
        viewOrderList.layer.borderWidth = 1
        viewOrderList.layer.borderColor = UIColor.black.cgColor
        
        btnInicio.layer.cornerRadius = btnInicio.frame.height/2
        btnInicio.layer.masksToBounds = false
        btnInicio.layer.borderWidth = 1
        btnInicio.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func pressSearch(_ sender: Any) {
        viewBack.isHidden = false
    }
    
    @IBAction func pressRecomendados(_ sender: Any) {
        let orderRecomendados = Constant.shoesResponse.objetos!.sorted(by: { $0.storeId! > $1.storeId! })
        Constant.shoesResponse.objetos = orderRecomendados
        listTableView.reloadData()
        viewBack.isHidden = true
    }
    @IBAction func pressPuntuación(_ sender: Any) {
        let orderScore = Constant.shoesResponse.objetos!.sorted(by: { $0.score! > $1.score! })
        Constant.shoesResponse.objetos = orderScore
        listTableView.reloadData()
        viewBack.isHidden = true
    }
    @IBAction func pressPrecio(_ sender: Any) {
        let orderPrecio = Constant.shoesResponse.objetos!.sorted(by: { $0.price! < $1.price! })
        Constant.shoesResponse.objetos = orderPrecio
        listTableView.reloadData()
        viewBack.isHidden = true
    }
    
    @objc func pressView(_ sender:UITapGestureRecognizer){
        viewBack.isHidden = true
    }
    
    @IBAction func pressInicio(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
}

extension ListStoreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constant.shoesResponse.objetos!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rawCell = tableView.dequeueReusableCell(
                    withIdentifier: String(describing: ShoesListTableViewCell.self),
                    for: indexPath)
        guard let cell = rawCell as? ShoesListTableViewCell else {
            assertionFailure("Can't find the expected cell")
            return rawCell
        }
        cell.object = Constant.shoesResponse.objetos![indexPath.row]
        cell.settings()
        
        return cell
    }
}
