//
//  ShoesListTableViewCell.swift
//  ShoesPhotography
//
//  Created by Everis on 5/02/22.
//

import UIKit

class ShoesListTableViewCell: UITableViewCell {

    @IBOutlet weak var viewTotalStore: UIView!
    
    @IBOutlet weak var imageShoes: UIImageView!
    @IBOutlet weak var imageHeart: UIImageView!
    @IBOutlet weak var viewShowOffert: UIView!
    
    @IBOutlet weak var amountPrice: UILabel!
    @IBOutlet weak var titleStore: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    var object: ObjectList!
    
    func settings() {
        imageShoes.layer.borderWidth = 1
        imageShoes.layer.borderColor = UIColor.black.cgColor
        
        viewTotalStore.layer.borderWidth = 1
        viewTotalStore.layer.borderColor = UIColor.black.cgColor
        
        amountPrice.text = "S/ " + object.price!
        titleStore.text = object.name
        
        
        let url = URL(string: object.url ?? "")
           if let data = try? Data(contentsOf: url!)
           {
            let image: UIImage = UIImage(data: data)!
            imageShoes.image = image
           }
        
        switch object.score {
            case "0":
                star1.alpha = 0
                star2.alpha = 0
                star3.alpha = 0
                star4.alpha = 0
                star5.alpha = 0
            case "1":
                star1.alpha = 1
                star2.alpha = 0
                star3.alpha = 0
                star4.alpha = 0
                star5.alpha = 0
            case "2":
                star1.alpha = 1
                star2.alpha = 1
                star3.alpha = 0
                star4.alpha = 0
                star5.alpha = 0
            case "3":
                star1.alpha = 1
                star2.alpha = 1
                star3.alpha = 1
                star4.alpha = 0
                star5.alpha = 0
            case "4":
                star1.alpha = 1
                star2.alpha = 1
                star3.alpha = 1
                star4.alpha = 1
                star5.alpha = 0
            default :
                star1.alpha = 1
                star2.alpha = 1
                star3.alpha = 1
                star4.alpha = 1
                star5.alpha = 1
        }
    }
    

}
