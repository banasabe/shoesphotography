//
//  LoaderViewController.swift
//  ShoesPhotography
//
//  Created by Aaron Amir Cordero Carranza on 14/02/22.
//

import UIKit

class LoaderViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    
        let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        myActivityIndicator.tag = 101
        view.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            if let viewWithTag = self.view.viewWithTag(101) {
                    viewWithTag.removeFromSuperview()
            }else{
                print("No!")
            }
            
            if UserDefaults.standard.object(forKey: "Usuario") != nil {
                self.performSegue(withIdentifier: "goToTabBar", sender: self)
            } else {
                self.performSegue(withIdentifier: "goToLogin", sender: self)
            }
        })
        super.viewWillAppear(animated)
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
    }
    
}
