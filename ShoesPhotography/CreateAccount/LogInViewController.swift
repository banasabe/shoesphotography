//
//  LogInViewController.swift
//  ShoesPhotography
//
//  Created by Aaron Amir Cordero Carranza on 13/02/22.
//

import UIKit

class LogInViewController: UIViewController {

    @IBOutlet weak var imageShoes: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var viewLoader: UIView!
    
    let networkManager = NetworkManager()
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        viewLoader.isHidden = true
        userNameTextField.text = ""
        passwordTextField.text = ""

        btnLogIn.isEnabled = false
        btnLogIn.backgroundColor = UIColor.red
        
        imageShoes.layer.borderWidth = 1
        imageShoes.layer.masksToBounds = false
        imageShoes.layer.borderColor = UIColor.black.cgColor
        imageShoes.layer.cornerRadius = imageShoes.frame.height/2
        imageShoes.clipsToBounds = true
        
        userNameTextField.autocorrectionType = .no
        passwordTextField.autocorrectionType = .no
        
        userNameTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        userNameTextField.layer.borderWidth = 1.0
        userNameTextField.layer.masksToBounds = false
        userNameTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        userNameTextField.layer.shadowOpacity = 1
        userNameTextField.layer.shadowRadius = 20

        passwordTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        passwordTextField.layer.borderWidth = 1.0
        passwordTextField.layer.masksToBounds = false
        passwordTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        passwordTextField.layer.shadowOpacity = 1
        passwordTextField.layer.shadowRadius = 20
        
        btnLogIn.layer.cornerRadius = btnLogIn.frame.height/2
        btnLogIn.layer.masksToBounds = false
        btnLogIn.layer.borderWidth = 1
        btnLogIn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        super.viewWillAppear(animated)
    }
    
    @IBAction func pressGoToRegister(_ sender: Any) {
        performSegue(withIdentifier: "goToRegister", sender: self)
    }
    
    func viewReloadService() {
        viewLoader.isHidden = false
        let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        viewLoader.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()
    }
    
    @IBAction func pressLogIn(_ sender: Any) {
        viewReloadService()
        networkManager.loginAccount(userName: userNameTextField.text!, password: passwordTextField.text!, success: {

            self.viewLoader.removeFromSuperview()
            
            UserDefaults.standard.set(self.userNameTextField.text!, forKey: "Usuario")
            UserDefaults.standard.set(self.passwordTextField.text!, forKey: "Contraseña")
            
            self.performSegue(withIdentifier: "goToInicio", sender: self)
        }, showMessage: {
            let refreshAlert = UIAlertController(title: "Alerta", message: "Credenciales inválidas, porfavor intente nuevamente.", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
              }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        })
    }
    
    func validateTextField() {
        if self.userNameTextField?.text! != "" && self.passwordTextField?.text! != "" {
            btnLogIn.isEnabled = true
            btnLogIn.backgroundColor = UIColor.green
        } else {
            btnLogIn.isEnabled = false
            btnLogIn.backgroundColor = UIColor.red
        }
    }
}


extension LogInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        validateTextField()
        if textField == userNameTextField {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateTextField()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
