//
//  CreateAccountViewController.swift
//  ShoesPhotography
//
//  Created by Aaron Amir Cordero Carranza on 13/02/22.
//

import UIKit

class CreateAccountViewController: UIViewController {

    @IBOutlet weak var imageCircule: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var btnCreateAccount: UIButton!
    
    @IBOutlet weak var viewLoader: UIView!
    let networkManager = NetworkManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        viewLoader.isHidden = true
        btnCreateAccount.isEnabled = false
        btnCreateAccount.backgroundColor = UIColor.red
        imageSetting()
        textFieldSetting()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 80
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 10
        }
    }
    
    func textFieldSetting() {
        nameTextField.autocorrectionType = .no
        lastNameTextField.autocorrectionType = .no
        emailTextField.autocorrectionType = .no
        userNameTextField.autocorrectionType = .no
        passwordTextField.autocorrectionType = .no
        
        nameTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        nameTextField.layer.borderWidth = 1.0
        nameTextField.layer.masksToBounds = false
        nameTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        nameTextField.layer.shadowOpacity = 1
        nameTextField.layer.shadowRadius = 20
        
        lastNameTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lastNameTextField.layer.borderWidth = 1.0
        lastNameTextField.layer.masksToBounds = false
        lastNameTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        lastNameTextField.layer.shadowOpacity = 1
        lastNameTextField.layer.shadowRadius = 20
        
        emailTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        emailTextField.layer.borderWidth = 1.0
        emailTextField.layer.masksToBounds = false
        emailTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        emailTextField.layer.shadowOpacity = 1
        emailTextField.layer.shadowRadius = 20

        userNameTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        userNameTextField.layer.borderWidth = 1.0
        userNameTextField.layer.masksToBounds = false
        userNameTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        userNameTextField.layer.shadowOpacity = 1
        userNameTextField.layer.shadowRadius = 20

        passwordTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        passwordTextField.layer.borderWidth = 1.0
        passwordTextField.layer.masksToBounds = false
        passwordTextField.layer.shadowColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        passwordTextField.layer.shadowOpacity = 1
        passwordTextField.layer.shadowRadius = 20
    }
    
    func imageSetting() {
        imageCircule.layer.borderWidth = 1
        imageCircule.layer.masksToBounds = false
        imageCircule.layer.borderColor = UIColor.black.cgColor
        imageCircule.layer.cornerRadius = imageCircule.frame.height/2
        imageCircule.clipsToBounds = true
        
        btnCreateAccount.layer.cornerRadius = btnCreateAccount.frame.height/2
        btnCreateAccount.layer.masksToBounds = false
        btnCreateAccount.layer.borderWidth = 1
        btnCreateAccount.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func viewReloadService() {
        viewLoader.isHidden = false
        let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        viewLoader.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()
    }
    
    @IBAction func pressCreateAccount(_ sender: Any) {
        viewReloadService()
        networkManager.createAccount(name: nameTextField.text!, lastName: lastNameTextField.text!, email: emailTextField.text!, userName: userNameTextField.text!, password: passwordTextField.text!, success: {
            self.viewLoader.removeFromSuperview()
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    func validateTextField() {
        if validateEmail(enteredEmail: emailTextField.text!) && self.nameTextField?.text! != "" && self.lastNameTextField?.text! != "" && self.userNameTextField?.text! != "" && self.passwordTextField?.text! != "" {
            btnCreateAccount.isEnabled = true
            btnCreateAccount.backgroundColor = UIColor.green
        } else {
            btnCreateAccount.isEnabled = false
            btnCreateAccount.backgroundColor = UIColor.red
        }
    }
}

extension CreateAccountViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        validateTextField()
        if textField == nameTextField || textField == lastNameTextField {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        } else if textField == userNameTextField {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateTextField()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}
