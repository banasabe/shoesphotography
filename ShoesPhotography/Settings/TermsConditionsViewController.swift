//
//  TermsConditionsViewController.swift
//  ShoesPhotography
//
//  Created by Aaron Amir Cordero Carranza on 13/02/22.
//

import UIKit

class TermsConditionsViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.backButtonTitle = " "
        navigationItem.backBarButtonItem?.title = " "
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        if Constant.settingNumber == 0 {
            navigationItem.title = "Términos y Condiciones"
            textView.text = "Ofertas válidas en Perú en las tiendas Sony Store en productos seleccionados bajo la campaña de “Compra en Perú al mismo precio de U.S.A. Los precios comparativos están basados en los precios publicados en el Web Site de B&H https://www.bhphotovideo.com/. El cálculo del precio es el siguiente: Precio publicado en dólares + el IVA del 10% y su posterior equivalencia en Moneda local usando la Tasa de Cambio promedio publicada en https://www.oanda.com/ del día hábil anterior a la fecha de consulta. Se calcula en períodos de tiempo especificados en el momento de compra; estos precios son de referencia y pueden variar sin previo aviso. Las cámaras y lentes Sony cuentan con garantía local de 1 año por defectos de fabricación. (*) En comparación a los cargos extras por compras en el extranjero."
        } else {
            navigationItem.title = "Cómo funciona la aplicación"
            textView.text = "Hoy en día todas aquellas personas que contamos con un dispositivo móvil solemos tener más de una aplicación que usamos frecuentemente. Ya sea para mirar el tiempo, noticias, leer o jugar entre otros. Algunas de ellas ya ocupan parte de nuestro día a día como pueden ser Whatsapp, Spotify, Youtube, Facebook o Instagram. Antes de ver cómo funcionan las aplicaciones móviles, vamos a hacer un breve resumen de qué son y cuáles son los sistemas operativos que hay. Es necesario adentrarnos en el mundo de las apps si queremos llegar a crear una. Si te interesa, sigue leyendo. ¿Qué es una aplicación móvil? Las apps son programas diseñados para poder ser ejecutados en nuestros dispositivos móviles, tablets, relojes inteligentes, etc. Permiten a sus usuarios acceder a información, realizar interacciones o acceder a servicios como ya hemos comentado anteriormente. Una aplicación móvil no se instala en un dispositivo, sino en un sistema operativo, los cuales veremos a continuación. El inicio de las apps se remonta a finales de los años 90. ¿Quién no recuerda los famosos juegos como el Snake y el Tetris o nuestros más queridos editores de tono de llamada? Haciendo una comparación con la actualidad, cumplían funciones muy básicas, pero que han marcado un antes y un después en el mercado de las apps. ¿Qué es un sistema operativo? Los sistemas operativos son el motor de las apps, aquellos programas que las hacen funcionar. Es por ello por lo que hemos comentado antes que una app se instala en estos y no en nuestros dispositivos en sí. Sin ellos nuestros dispositivos no podrían funcionar ya que son los encargados del funcionamiento de nuestra pantalla, micrófono, cámara, etc., es decir, de todo."
        }
        super.viewDidLoad()
    }
    
    
}
