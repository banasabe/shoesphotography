//
//  SettingsViewController.swift
//  ShoesPhotography
//
//  Created by Aaron Amir Cordero Carranza on 13/02/22.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.setNavigationBarHidden(true, animated: animated)
        buttonSettings()
        super.viewWillAppear(animated)
    }
    
    func buttonSettings() {
        btn1.layer.cornerRadius = btn1.frame.height/2
        btn1.layer.masksToBounds = false
        btn1.layer.borderWidth = 1
        btn1.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btn2.layer.cornerRadius = btn2.frame.height/2
        btn2.layer.masksToBounds = false
        btn2.layer.borderWidth = 1
        btn2.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btn3.layer.cornerRadius = btn3.frame.height/2
        btn3.layer.masksToBounds = false
        btn3.layer.borderWidth = 1
        btn3.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        btn4.layer.cornerRadius = btn4.frame.height/2
        btn4.layer.masksToBounds = false
        btn4.layer.borderWidth = 1
        btn4.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func pressButton1(_ sender: Any) {
        Constant.settingNumber = 0
        performSegue(withIdentifier: "goToSettings", sender: self)
    }
    
    @IBAction func pressButton2(_ sender: Any) {
        Constant.settingNumber = 1
        performSegue(withIdentifier: "goToSettings", sender: self)
    }
    
    @IBAction func pressButton4(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "Deseas Cerrar tu Sesión?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action: UIAlertAction!) in
            UserDefaults.standard.removeObject(forKey: "Usuario")
            UserDefaults.standard.removeObject(forKey: "Contraseña")
            self.performSegue(withIdentifier: "backToLogin", sender: self)
            print("Cierre de Sesion Exitosa")
          }))

        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
          print("Handle Cancel Logic here")
          }))

        present(refreshAlert, animated: true, completion: nil)
    }
}
